<?php

class Product
{
	public function __construct($id, $config)
	{
		foreach ($config as $key => $value)
			$this->$key = $value;

		$this->id = $id;
	}

	public function __toString()
	{
		return $this->title;
	}
}

function get_products()
{
	$search_paths = array(
		'/home/cover/bestuur/Website/Merchandise/products.json',
		dirname(__FILE__) . '/../products.json'
	);

	foreach ($search_paths as $path)
	{
		if (file_exists($path))
		{
			$product_file = $path;
			break;
		}
	}

	$json = file_get_contents($product_file);

	if (!$json)
		throw new Exception('Could not load products');

	$products = json_decode($json);

	if (!$products)
		throw new Exception('Could not parse product file');

	foreach ($products as $id => &$product)
		$product = new Product($id, $product);

	return $products;
}

function get_product($id)
{
	static $products;
	
	if (!$products)
		$products = get_products();

	return isset($products->$id) ? $products->$id : null;
}

function nsprintf($singular, $plural, $n)
{
	return sprintf($n === 1 ? $singular : $plural, $n);
}

function render_template()
{
	ob_start();
	extract(func_get_arg(1));
	include func_get_arg(0);
	return ob_get_clean();
}
