<?php

session_start();

class Order
{
	public $product;

	public $options;

	public function __toString()
	{
		$options = array();

		foreach ($this->options as $key => $value)
			$options[] = $key . ': ' . $value;

		return strval($this->product) . ":\n" . implode("\n", $options);
	}
}

function add_to_cart($product_id, array $options)
{
	$product = get_product($product_id);

	if (!$product)
		throw new Exception('Could not find product');

	if (empty($_SESSION['cart']))
		$_SESSION['cart'] = array();

	$errors = array();

	if (isset($product->options))
	{
		foreach ($product->options as $option => $config)
		{
			if (!isset($options[$option]))
				$errors[] = $option;

			if (!in_array($options[$option], array_keys((array) $config->values)))
				$errors[] = $option;

			// Todo: check inter-option restrictions
		}
	}

	if (!empty($errors))
		throw new FormException('Not all options where provided correctly', $errors);

	$_SESSION['cart'][uniqid()] = array(
		'product_id' => $product_id,
		'options' => $options
	);
}

function remove_from_cart($item_id)
{
	unset($_SESSION['cart'][$item_id]);
}

function get_cart()
{
	$cart = array();

	if (!isset($_SESSION['cart']) || !is_array($_SESSION['cart']))
		$_SESSION['cart'] = array();

	foreach ($_SESSION['cart'] as $item_id => $product_config)
	{
		$cart[$item_id] = new Order();
		$cart[$item_id]->product = get_product($product_config['product_id']);
		$cart[$item_id]->options = $product_config['options'];
	}

	return $cart;
}

function get_cart_item_count()
{
	return isset($_SESSION['cart']) ? count($_SESSION['cart']) : 0;
}

function get_cart_price()
{
	$sum = 0;

	foreach (get_cart() as $item)
		$sum += $item->product->price;

	return $sum;
}

function order_cart($personal_details)
{
	$errors = array();

	if (strlen($personal_details['name']) < 2)
		$errors[] = 'name';

	if (strlen($personal_details['address']) < 5)
		$errors[] = 'address';

	if (strlen($personal_details['place']) < 2)
		$errors[] = 'place';

	if (!preg_match('/^\d{4}\s*[a-z]{2}$/i', $personal_details['postal_code']))
		$errors[] = 'postal_code';

	if (strpos($personal_details['email_address'], '@') === false
		|| strpos($personal_details['email_address'], '.') === false)
		$errors[] = 'email_address';

	if (!preg_match('/^[A-Z]{2}\d{2}[A-Z]{4}\d{10}$/i', $personal_details['bank_account']))
		$errors[] = 'bank_account';

	if (empty($personal_details['agree']) || $personal_details['agree'] != 'yes')
		$errors[] = 'agree';

	if (count($errors) > 0)
		return compact('errors');

	$mail = "A new order has been submitted:\n"
		. implode("\n\n", get_cart()) . "\n\n"
		. "Personal details:\n"
		. print_r($personal_details, true);

	$headers = array(
		'From: ' . $personal_details['email_address'],
		'Cc: ' . $personal_details['email_address']);

	mail('nepshop@ikhoefgeen.nl', 'New merchandise order', $mail, implode("\r\n", $headers));

	$_SESSION['cart'] = array();

	return array('errors' => array());
}
