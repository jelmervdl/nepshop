/* Product page */
window.jQuery(function ($) {
	"use strict";
	
	var popup = $('<div class="popup">').appendTo('body'),
		popupBackdrop = $('<div class="popup-backdrop">').appendTo(popup),
		popupWindow = $('<div class="popup-window">').appendTo(popup),
		popupLoading = $('<div class="popup-loading-message">Bezig met laden van product…</div>').appendTo(popupWindow),
		popupContent = $('<div class="popup-content">').appendTo(popupWindow),
		popupCloseButton = $('<a href="#" class="close">&times;</a>').appendTo(popupWindow);

	popup.find('.popup-backdrop, .close').click(function (e) {
		popup.removeClass('visible');
		e.preventDefault();
	});

	function showPopup(href, contentName) {
		popupLoading.text('Bezig met ' + contentName + '…');

		popup.removeClass('loading loaded').addClass('visible');

		var showLoadingTimer = setTimeout(function () {
			popup.addClass('loading');
		}, 200);

		popupContent.html('').load(href, function () {
			clearTimeout(showLoadingTimer);
			popup.removeClass('loading').addClass('loaded');
			popup.trigger('load');
		});
	}

	function hidePopup() {
		popup.removeClass('visible');
	}

	function updateCartItemCount(itemCount) {
		$('.cart-item-count').each(function () {
			$(this).text($(this).data(itemCount === 1 ? 'format-singular' : 'format-plural').replace('%d', itemCount));
		});
	}

	function formatMoney(price) {
		return Math.floor(price) + ',' + ('00' + Math.floor((price - Math.floor(price)) * 100)).substr(-2);
	}

	$(document).on('click', "a[href^='product.php']", function (e) {
		if (e.metaKey || e.shiftKey || e.altKey || e.ctrlKey)
			return;

		var productName = $(this).closest('.product').find('.title').text();
		showPopup(this.href + ' .product-details', productName);
		e.preventDefault();
	});

	$(document).on('click', "a[href^='cart.php']", function (e) {
		if (e.metaKey || e.shiftKey || e.altKey || e.ctrlKey)
			return;

		showPopup(this.href + ' .cart-contents', 'cart');
		e.preventDefault();
	});

	$(document).keyup(function (e) {
		if (e.keyCode === 27 && popup.hasClass('visible')) {
			e.preventDefault();
			hidePopup();
		}
	});

	/* Order page and popup */

	$(document).on('submit', 'form.add-to-cart-form', function (e) {
		var form = $(this);
		e.preventDefault();
		$.post('cart-api.php', form.serialize(), function (response) {
			if (response.error) {
				window.alert(response.message);
			} else {
				updateCartItemCount(response.cart_item_count);
				var message = $('<span class="added-to-cart-message">Toegevoegd! <a href="index.php">Ga verder met winkelen</a> of <a href="cart.php">naar de kassa</a>.</span>')
					.find("a[href='index.php']").click(function (e) {
						e.preventDefault();
						hidePopup();
					}).end();
				
				if (form.find('.added-to-cart-message').replaceWith(message).length === 0) {
					message.insertAfter(form.find('button[type=submit]'));
				}
			}
		});
	});

	function updateProductOptions(form) {
		$(form).find('[data-restrictions]').each(function() {
			var restrictions = $(this).data('restrictions');
			var PROPERTY = 0, OPERATOR = 1, VALUE = 2;
			var result = true;

			for (var i = 0; i < restrictions.length && result; ++i) {
				var lhs = $(form).find('input[type=radio]:checked').filter(function() {
					return this.name == 'options[' + restrictions[i][PROPERTY] + ']';
				});

				var rhs = restrictions[i][VALUE];

				result = lhs.filter(function() {
					switch (restrictions[i][OPERATOR]) {
						case '=':
							return this.value == rhs;

						case '<':
							return this.value < rhs;

						case '>':
							return this.value > rhs;

						case '!=':
						case '<>':
							return this.value != rhs;

						default:
							throw Error("Unknown operator '" + restrictions[i][OPERATOR] + "'");
					}
				}).length > 0;
			}
			
			if (!result) this.checked = false;

			this.disabled = !result;
		});
	}

	$(popup).on('load', function() {
		updateProductOptions(popup.find('form'));
	});

	$(document).on('change', 'form.add-to-cart-form input[type=radio]', function(e) {
		updateProductOptions(this.form)
	});

	$(document).on('click', 'form.remove-item-form button.remove-button', function (e) {
		e.preventDefault();

		var itemId = $(this).val();

		$.post('cart-api.php',
			{
				'action': 'remove',
				'item': itemId
			},
			function (response) {
				if (response.error) {
					window.alert(response.error);
				} else {
					// Update items in cart counts on all pages
					updateCartItemCount(response.cart_item_count);

					// Update the total sum in the footer of the table with the order
					$('.cart-contents table > tfoot > tr > th.price').html('&euro;&nbsp;' + formatMoney(response.cart_price));

					// Show or hide the order form if there are any items in the order left
					$('.cart-contents .order-form').toggleClass('hidden', response.cart_item_count === 0);

					// Remove the removed item from the order list
					$('#item-' + itemId).remove();
				}
			});
	});

	$(document).on('submit', 'form.order-form', function (e) {
		e.preventDefault();

		var form = $(this),
			message = $('<p>').text('Je bestelling wordt geplaatst…').insertAfter(form),
			button = $('<button>').attr('type', 'button').addClass('done-button').text('OK Doei!').insertAfter(message);

		// Hide the contents
		form.closest('.cart-contents').find('form').hide();

		// On clicking the done-button, hide the popup entirely.
		button.click(hidePopup).hide();

		// Do that submit 'n shit.
		$.post('cart-api.php', form.serialize(), function (response) {
			// If there are errors, show the forms again
			if (response.errors && response.errors.length > 0) {
				message.remove();
				form.show();
				form.find('[name]').removeClass('invalid');
				$.each(response.errors, function () {
					form.find("[name='" + this + "']").addClass('invalid');
				});
			} else {
				message.text('Je bestelling is geplaatst en je krijgt zo een e-mail ter bevestiging.');
				button.show();
			}
		});
	});

	var updateProductThumbnails = function(root)
	{
		var $picture = $(root).find('.product-pictures .picture');
		var $details = $(root).find('.product-pictures .picture-detail').closest('a');

		if ($details.length === 0)
			return;

		$picture.data('large', $details.first().attr('href'));

		if ($details.length === 1) {
			$details.first().hide();
			return;
		}

		$details.first().addClass('current');

		$details.on('click', function(e) {
			$picture.attr('src', this.href);
			$picture.data('large', this.href);
			$details.removeClass('current');
			$(this).addClass('current');
			e.preventDefault();
		});
	};

	updateProductThumbnails(document);

	$(popup).on('load', function() {
		updateProductThumbnails(popup);
	});

	var $preview = $('<div class="product-preview">');
	var delay = 300;
	var detachDelay;

	$(document).on('mouseover', '.product-details .picture', function(e) {
		var image = e.target;

		if (!$(image).data('large'))
			return;

		clearTimeout(detachDelay);

		$preview.css('opacity', 0);
		$preview.css('background-image', 'url(' + $(image).data('large') + ')');
		$preview.css('background-size', '200%');

		// Delay fade-in to prevent annoyances
		setTimeout(function() { $preview.css('opacity', 1); }, delay);

		$(image).closest('.product-details').append($preview);

		$(image).on('mousemove', function(e) {
			var offset = $(image).offset();
			var x = (e.pageX - offset.left) / image.width,
				y = (e.pageY - offset.top) / image.height;

			$preview.css('background-position', (x * 100) + '% ' + (y * 100) + '%');
		});

		$(image).on('mouseout', function() {
			$(image).off('mousemove');
			$(image).off('mouseout');
			$preview.css('opacity', 0);

			// Wait for fade-out css transition to finish
			delay = 0; // When hovering again inside the 500ms window, don't delay again.
			detachDelay = setTimeout(function() {
				delay = 300;
				$preview.detach();
			}, 500);
		});
	});
	
	var fancyHeader = function() {
		var ranges = {
			'.website-header .logo': {
				'transform': [0, -68, 1.0, 0.32,'translateX(%fpx) scale(%f)'],
				'margin-right': [18, 0, '%fpx']
			},

			'.website-header .cart': {
				'transform': [0, 20, 1.0, 0.75, 'translateX(%fpx) scale(%f)']
			},

			'.website-header h1': {
				'transform': [0, -35, 'translateX(%fpx)']
			},

			'.website-header h1 a': {
				'transform': [1.0, 0.75, 'scale(%f)']
			},

			'.website-header': {
				// 'font-size': [32, 24, '%fpx'],
				'height': [96, 48, '%fpx'],
				'box-shadow': [0, 2, '0 %fpx 5px rgba(0,0,0,0.26)']
			},

			'.popup > .popup-window': {
				'top': [65, 20, '%fpx']
			}
		};

		var calculateValue = function(range, run) {
			return range[1] + (range[0] - range[1]) * run;
		};

		var vsprintf = function(format, values) {
			for (var i = 0; i < values.length; ++i)
				format = format.replace('%f', values[i]);
			return format;
		};

		var prevrun = null;

		var repaintHeader = function() {
			requestAnimationFrame(function() {
				var run = 1 - Math.min($(window).scrollTop(), 48) / 48;

				if (run > 1)
					run = 1 + Math.log(run) / Math.log(4);

				if (run == prevrun)
					return;

				prevrun = run;

				for (var selector in ranges) {
					var $el = $(selector);

					for (var property in ranges[selector]) {
						var range = ranges[selector][property];
						var values = [];
						for (var i = 0; i < (range.length - 1) / 2; i++)
							values[i] = calculateValue(range.slice(i * 2, i * 2 + 2), run);
						$el.css(property, vsprintf(range[range.length - 1], values));
					}
				}
			});
		};

		var resetHeader = function() {
			for (var selector in ranges)
				$(selector).attr('style', '');

			prevrun = null;
		};

		var registered = false;

		var updateRepaintHeaderListener = function() {
			if (!registered && $(window).width() > 720) {
				$(window).on('scroll', repaintHeader);
				repaintHeader();
				registered = true;
			}
			else if (registered && $(window).width() <= 720) {
				$(window).off('scroll', repaintHeader);
				resetHeader();
				registered = false;
			}
		};

		$(window).resize(updateRepaintHeaderListener);

		updateRepaintHeaderListener();
	};

	if (window.requestAnimationFrame)
		fancyHeader();
});
