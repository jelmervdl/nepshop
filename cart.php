<?php
require 'include/init.php';
require 'include/cart.php';

function format_options($item)
{
	$options = array();

	foreach ($item->options as $option => $value)
		$options[] = sprintf('<dt>%s</dt><dd>%s</dd>',
			$item->product->options->{$option}->label,
			$item->product->options->{$option}->values->{$value}->label);

	return implode($options);
}

$title = 'Cart';

$cart = get_cart();

$content = render_template('templates/cart.phtml', compact('cart'));

echo render_template('templates/layout.phtml', compact('title', 'content'));
