<?php
require 'include/init.php';
require 'include/cart.php';

$title = 'Merchandise';

$content = render_template('templates/products.phtml', array('products' => get_products()));

echo render_template('templates/layout.phtml', compact('title', 'content'));
